# PyOpenGL scene renderer

Basic OpenGL scene renderer.

The end goal is to support:
 - Basic shapes: cubes, pyramids, etc.
 - OBJ reader
 - Basic shaders (MVP processing) and mesh processing (rotations, translations, scaling etc.)
 - Basic camera
 - Basic keyboard controls (move camera, move objects, wireframe objects etc.)
 - Depth vs color keybind
 - Ability to set waypoints for a camera and store pictures
 - Basic lighting/shadowing
 - Efficiency algorithms: culling etc. (optional)
