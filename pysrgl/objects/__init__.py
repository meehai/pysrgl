from .scene_object import SceneObject
from .mvp import MVPObject
from .one_color import OneColorObject
from .colored import ColoredObject
from .textured import TexturedObject
from .text import Text, FPSText
