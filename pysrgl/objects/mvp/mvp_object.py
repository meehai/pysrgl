from typing import Dict
import numpy as np
from overrides import overrides
from OpenGL.GL import *
from pathlib import Path

from ..scene_object import SceneObject, Scene
from ...utils.logger import logger
from ...utils.matrices import translationMatrix, scaleMatrix, rotationMatrix

class MVPObject(SceneObject):
    baseDir = Path(__file__).absolute().parent
    shaderName = "mvp"
    VS, FS = baseDir / "vertex_shader.glsl", baseDir / "fragment_shader.glsl"

    def __init__(self, buffers: Dict[str, np.ndarray]):
        super().__init__(buffers, shaderName=MVPObject.shaderName)
        self.resetPosition()

    @property
    def modelMatrix(self) -> np.ndarray:
        """Simple model matrix given by translation * rotation * scaling."""
        res = translationMatrix([self.dx, self.dy, self.dz]) \
            @ rotationMatrix([self.rx, self.ry, self.rz]) \
            @ scaleMatrix([self.sx, self.sy, self.sz])
        res = np.array(res, dtype=np.float32)
        return res

    def resetPosition(self):
        """Resets the object to the default position as given by vertices"""
        self.dx, self.dy, self.dz = 0, 0, 0
        self.rx, self.ry, self.rz = 0, 0, 0
        self.sx, self.sy, self.sz = 1, 1, 1

    def translate(self, dx: float, dy: float, dz: float):
        self.dx += dx
        self.dy += dy
        self.dz += dz

    def rotate(self, rx: float, ry: float, rz: float):
        self.rx += rx
        self.ry += ry
        self.rz += rz

    def scale(self, sx: float, sy: float = None, sz: float = None):
        if sy == None:
            sy = sx
        if sz == None:
            sz = sx
        if self.sx + sx >= 0:
            self.sx += sx
        else:
            logger.debug("Cannot scale down further on x")
        if self.sy + sy >= 0:
            self.sy += sy
        else:
            logger.debug("Cannot scale down further on y")
        if self.sz + sz >= 0:
            self.sz += sz
        else:
            logger.debug("Cannot scale down further on z")

    """Basic MVP object with default green color defined in shader"""
    @overrides
    def initialize(self, scene: Scene):
        _shader = scene.shaders[self.shaderName].shaderProgram
        self._positionId = glGetAttribLocation(_shader, "position")
        self._modelId = glGetUniformLocation(_shader, "model")
        self._viewId = glGetUniformLocation(_shader, "view")
        self._projectionId = glGetUniformLocation(_shader, "projection")

    @overrides
    def draw(self, scene: Scene):
        glUniformMatrix4fv(self._modelId, 1, False, self.modelMatrix)
        glUniformMatrix4fv(self._viewId, 1, False, np.linalg.inv(scene.activeCamera.cameraMatrix))
        glUniformMatrix4fv(self._projectionId, 1, False, scene.projectionMatrix)

        self.buffers["vertex"].bind()
        self.buffers["index"].bind()
        # Apply the Model-View-Projection (MVP) transform to this object.
        glEnableVertexAttribArray(0)
        glVertexAttribPointer(self._positionId, size=3, type=GL_FLOAT, normalized=False, stride=0, pointer=None)
