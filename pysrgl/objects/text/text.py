from overrides import overrides
from typing import Tuple, Optional
import freetype
import numpy as np
from OpenGL.GL import *
from OpenGL.GLU import *
from pathlib import Path

from ...utils.logger import logger
from ..scene_object import SceneObject, Scene

class CharacterSlot:
    def __init__(self, texture, glyph):
        self.texture = texture
        self.textureSize = (glyph.bitmap.width, glyph.bitmap.rows)

        if isinstance(glyph, freetype.GlyphSlot):
            self.bearing = (glyph.bitmap_left, glyph.bitmap_top)
            self.advance = glyph.advance.x
        elif isinstance(glyph, freetype.BitmapGlyph):
            self.bearing = (glyph.left, glyph.top)
            self.advance = None
        else:
            raise RuntimeError('unknown glyph type')

def _get_rendering_buffer(xpos: float, ypos :float, w: float, h: float) -> np.ndarray:
    return np.asarray([
        xpos,     ypos - h, 0, 0,
        xpos,     ypos,     0, 1,
        xpos + w, ypos,     1, 1,
        xpos,     ypos - h, 0, 0,
        xpos + w, ypos,     1, 1,
        xpos + w, ypos - h, 1, 0
    ], np.float32)

class Text(SceneObject):
    baseDir = Path(__file__).absolute().parent
    shaderName = "text"
    VS, FS = baseDir / "vertex_shader.glsl", baseDir / "fragment_shader.glsl"
    defaultFontFile = baseDir.parents[2] / "resources/fonts/Vera.ttf"
    assert defaultFontFile.exists(), defaultFontFile

    def __init__(self, text: str, position: Tuple[int, int], scale: float, \
            color: Tuple[int, int, int], fontfile: Optional[Path] = None):
        super().__init__({"vertex": []}, shaderName=Text.shaderName)
        if fontfile is None:
            logger.debug(f"No font file provided. Using default: '{Text.defaultFontFile}'")
            fontfile = Text.defaultFontFile

        self.text = text
        self.position = position
        self.scale = scale
        self.color = color
        self.Characters = {}

        self.face = freetype.Face(str(fontfile))
        self.face.set_char_size( 48*64 )

    @property
    def color(self):
        return self._color
    
    @color.setter
    def color(self, _color: Tuple[int, int, int]):
        assert len(_color) == 3
        assert 0 <= _color[0] <= 255 and 0 <= _color[1] <= 255 and 0 <= _color[2] <= 255
        self._color = _color

    @overrides
    # @param[in] scene The main scene
    # @param[in] name The name under which this object is known in the scene 
    def initialize(self, scene):
        glPixelStorei(GL_UNPACK_ALIGNMENT, 1)

        for i in range(0, 128):
            self.face.load_char(chr(i))
            glyph = self.face.glyph
            #generate texture
            texture = glGenTextures(1)
            glBindTexture(GL_TEXTURE_2D, texture)
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, glyph.bitmap.width, glyph.bitmap.rows, 0,
                        GL_RED, GL_UNSIGNED_BYTE, glyph.bitmap.buffer)

            #texture options
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE)
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE)
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)

            #now store character for later use
            self.Characters[chr(i)] = CharacterSlot(texture,glyph)

        glBindTexture(GL_TEXTURE_2D, 0)

        #configure VAO/VBO for texture quads
        self.VAO = glGenVertexArrays(1)
        glBindVertexArray(self.VAO)
        
        self.VBO = glGenBuffers(1)
        glBindBuffer(GL_ARRAY_BUFFER, self.VBO)
        glBufferData(GL_ARRAY_BUFFER, 6 * 4 * 4, None, GL_DYNAMIC_DRAW)
        glEnableVertexAttribArray(0)
        glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, None)
        glBindBuffer(GL_ARRAY_BUFFER, 0)
        glBindVertexArray(0)

        self._textColorId = glGetUniformLocation(scene.shaders[self.shaderName].shaderProgram, "textColor")

    @overrides
    def draw(self, scene: Scene):
        glUniform3f(self._textColorId, self._color[0] / 255, self.color[1] / 255, self.color[2] / 255)

        glActiveTexture(GL_TEXTURE0)
        glEnable(GL_BLEND)
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)

        glBindVertexArray(self.VAO)
        x, y = self.position
        for c in self.text:
            ch = self.Characters[c]
            w, h = ch.textureSize
            w = w*self.scale
            h = h*self.scale
            vertices = _get_rendering_buffer(x,y,w,h)

            #render glyph texture over quad
            glBindTexture(GL_TEXTURE_2D, ch.texture)
            #update content of VBO memory
            glBindBuffer(GL_ARRAY_BUFFER, self.VBO)
            glBufferSubData(GL_ARRAY_BUFFER, 0, vertices.nbytes, vertices)

            glBindBuffer(GL_ARRAY_BUFFER, 0)
            #render quad
            glDrawArrays(GL_TRIANGLES, 0, 6)
            #now advance cursors for next glyph (note that advance is number of 1/64 pixels)
            x += (ch.advance>>6)*self.scale

        glDisable(GL_BLEND)
        glBindVertexArray(0)
        glBindTexture(GL_TEXTURE_2D, 0)

    @overrides
    def getNumIndices(self) -> int:
        return 0
    
    @overrides
    def getNumVertices(self) -> int:
        return 0
