from typing import Tuple, Optional
from overrides import overrides
from pathlib import Path
from .text import Text, Scene

class FPSText(Text):
    def __init__(self, position: Tuple[int, int], scale: float, \
            color: Tuple[int, int, int], fontfile: Optional[Path] = None):
        super().__init__(None, position, scale, color, fontfile)

    @overrides
    def draw(self, scene: Scene):
        self.text = f"FPS: {scene.fps:.2f}"
        super().draw(scene)
