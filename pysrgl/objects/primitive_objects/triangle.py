import numpy as np
from typing import Tuple

def triangle(center: Tuple[float, float], radius: float) -> Tuple[np.ndarray, np.ndarray]:
    vertices = np.float32([
        (center[0] - radius, center[1] + radius, 0.0),
        (center[0] + radius, center[1] + radius, 0.0),
        (center[0], center[1] - radius, 0.0),
    ])

    indices = np.int32([
        (0, 1, 2)
    ])

    return {"vertex": vertices, "index": indices}
