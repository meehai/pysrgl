import numpy as np
from typing import Tuple
from .rectangle import rectangle

def horizontalLine(startPosition: float, length: float, thickness: float=0.01) -> Tuple[np.ndarray, np.ndarray]:
        topLeft = startPosition
        bottomRight = (startPosition[0] + length, startPosition[1] + thickness)
        return rectangle(topLeft, bottomRight)
