from typing import Dict, Optional
import numpy as np
from overrides import overrides
from OpenGL.GL import *
from pathlib import Path
from ..mvp import MVPObject
from ..scene_object import SceneObject, Scene

class ColoredObject(MVPObject):
    """
    An MVP object with a provided color buffer.
    """
    baseDir = Path(__file__).absolute().parent
    shaderName = "coloredObject"
    VS, FS = baseDir / "vertex_shader.glsl", baseDir / "fragment_shader.glsl"

    def __init__(self, buffers: Dict[str, np.ndarray], colors: Optional[np.ndarray] = None):
        if colors is not None:
            assert not "colors" in buffers
            buffers["colors"] = colors
        SceneObject.__init__(self, buffers, ColoredObject.shaderName)
        self.resetPosition()
        self.colors = buffers["colors"]

    @overrides
    def initialize(self, scene: Scene):
        super().initialize(scene)
        _shader = scene.shaders[self.shaderName].shaderProgram
        self._colorId = glGetAttribLocation(_shader, "color")

    @overrides
    def draw(self, scene: Scene):
        super().draw(scene)
        # Colors
        self.buffers["colors"].bind()
        glEnableVertexAttribArray(1)
        glVertexAttribPointer(self._colorId, size=3, type=GL_FLOAT, normalized=False, stride=0, pointer=None)
