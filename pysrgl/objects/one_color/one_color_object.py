from typing import Dict, Tuple
import numpy as np
from overrides import overrides
from OpenGL.GL import *
from pathlib import Path
from ..mvp import MVPObject
from ..scene_object import SceneObject, Scene

class OneColorObject(MVPObject):
    """
    An MVP object with a provided unique color.
    """
    baseDir = Path(__file__).absolute().parent
    shaderName = "oneColorObject"
    VS, FS = baseDir / "vertex_shader.glsl", baseDir / "fragment_shader.glsl"

    def __init__(self, buffers: Dict[str, np.ndarray], color: Tuple[int, int, int]):
        SceneObject.__init__(self, buffers, OneColorObject.shaderName)
        self.resetPosition()
        self.color = color

    @property
    def color(self):
        return self._color

    @color.setter
    def color(self, _color: Tuple[int, int, int]):
        assert len(_color) == 3
        self._color = _color

    @overrides
    def initialize(self, scene: Scene):
        _shader = scene.shaders[self.shaderName].shaderProgram
        self._positionId = glGetAttribLocation(_shader, "position")
        self._modelId = glGetUniformLocation(_shader, "model")
        self._viewId = glGetUniformLocation(_shader, "view")
        self._projectionId = glGetUniformLocation(_shader, "projection")
        self._colorId = glGetUniformLocation(_shader, "objectColor")

    @overrides
    def draw(self, scene: Scene):
        super().draw(scene)
        glUniform3f(self._colorId, self.color[0] / 255, self.color[1] / 255, self.color[2] / 255)
