#version 330 core

uniform vec3 objectColor;
out vec3 FS_Color;

void main()
{
    FS_Color = objectColor;
}
