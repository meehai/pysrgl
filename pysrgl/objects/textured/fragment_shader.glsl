#version 330 core

in vec2 VS_UV;
uniform sampler2D FS_Sampler;
out vec3 FS_Color;

void main()
{
    FS_Color = texture( FS_Sampler, VS_UV ).rgb;
}
