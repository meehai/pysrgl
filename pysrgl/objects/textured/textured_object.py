import numpy as np
from typing import Dict, Optional
from overrides import overrides
from OpenGL.GL import *
from pathlib import Path
from ..mvp import MVPObject
from ..scene_object import SceneObject, Scene

class TexturedObject(MVPObject):
    """
    An MVP object with a provided texture UV and texture name.
    """
    baseDir = Path(__file__).absolute().parent
    shaderName = "texturedObject"
    VS, FS = baseDir / "vertex_shader.glsl", baseDir / "fragment_shader.glsl"

    def __init__(self, buffers: Dict[str, np.ndarray], textureName: str, textureUV: Optional[np.ndarray] = None):
        if textureUV is not None:
            assert not "textureUV" in buffers
            buffers["uv"] = textureUV
        SceneObject.__init__(self, buffers, TexturedObject.shaderName)
        self.resetPosition()
        self.textureName = textureName
        self.textureUV = buffers["uv"]

    @overrides
    def initialize(self, scene: Scene):
        super().initialize(scene)
        _shader = scene.shaders[self.shaderName].shaderProgram
        self._uvId = glGetAttribLocation(_shader, "uv")
        self._samplerId = glGetUniformLocation(_shader, "FS_Sampler")
        self._textureId = scene.textures[self.textureName]
        print(self._uvId, self._samplerId, self._textureId)

    def draw(self, scene):
        super().draw(scene)

        glActiveTexture(GL_TEXTURE0)
        glBindTexture(GL_TEXTURE_2D, self._textureId)
        glUniform1i(self._samplerId, 0)

        glEnableVertexAttribArray(1)
        self.buffers["uv"].bind()
        glVertexAttribPointer(1, size=2, type=GL_FLOAT, normalized=False, stride=0, pointer=None)
