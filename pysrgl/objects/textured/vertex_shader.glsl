#version 330 core

layout(location = 0) in vec4 position;
layout(location = 1) in vec2 uv;
uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
out vec2 VS_UV;

void main()
{
    gl_Position = projection * view * model * position;
    VS_UV = uv;
}
