import numpy as np
from .utils.matrices import rotationMatrix, translationMatrix

class Camera:
    def __init__(self):        
        self.cameraTranslation = np.array([0, 0, 0], dtype=np.float32)
        self.cameraRotation = np.array([0, 0, 0], dtype=np.float32)

    @property
    def cameraMatrix(self) -> np.ndarray:
        res = translationMatrix(self.cameraTranslation) @ rotationMatrix(self.cameraRotation)
        res = np.array(res, dtype=np.float32).data
        return res

    def translate(self, delta: np.ndarray):
        self.cameraTranslation += delta

    def rotate(self, delta: np.ndarray):
        self.cameraRotation = (self.cameraRotation + delta) % (2 * np.pi)

    def reset(self):
        self.cameraTranslation = np.array([0, 0, 0], dtype=np.float32)
        self.cameraRotation = np.array([0, 0, 0], dtype=np.float32)
