from typing import Dict, List
from pathlib import Path

class ObjLoader:
    def __init__(self,fname):
        self.buffers_ibo = self.read(fname)
        self.buffers = self.buildBufferNonIBO()
 
    def read(self, fname: str) -> Dict[str, List[float]]:
        f = open(str(Path(fname).absolute()),"r") # in text mode

        vertexs, indices, normals, texcoords = [], [], [], []

        for line in f:
            if line.startswith('#'): continue
            items = line.split()
            if len(items) == 0:
                continue

            if(items[0]=="v"):
                v = map(float, items[1:4])
                vertexs.extend(v)
            elif(items[0]=="vn"):
                v = map(float, items[1:4])
                normals.extend(v)
            elif(items[0]=="vt"):
                v = map(float, items[1:3])
                texcoords.extend(v)
            elif(items[0]=="f"):
                index = map(int,items[1].split("/"))
                indices.extend(index)  
                index = map(int,items[2].split("/"))
                indices.extend(index)
                index = map(int,items[3].split("/"))
                indices.extend(index)
            elif(items[0]=="s"):
                print(f"Skiping smooth line: {line}")
            elif(items[0]=="mtllib"):   
                print(f"Skiping refernce material line: {line}")
            elif(items[0]=="usemtl"):   
                print(f"Skipping material line: {line}")
            else:
                print(f"skip unknown line : {line[0:-1]}")
        return {"vertex": vertexs, "index": indices, "normals": normals, "uv": texcoords}

    def buildBufferNonIBO(self) -> Dict[str, List[float]]:
        res = {"vertex": [], "uv": [], "normals": []}

        for i in range(0,len(self.buffers_ibo["index"]),3):
            index = 3*(self.buffers_ibo["index"][i]-1)
            res["vertex"].extend(self.buffers_ibo["vertex"][index:index+3])
            index = 2*(self.buffers_ibo["index"][i+1]-1)
            res["uv"].extend(self.buffers_ibo["uv"][index:index+2])
            index = 3*(self.buffers_ibo["index"][i+2]-1)
            res["normals"].extend(self.buffers_ibo["normals"][index:index+3])
        return res
