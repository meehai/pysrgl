from typing import Dict, Union
from pathlib import Path
from OpenGL.GL import *
from OpenGL.GL import shaders
from .logger import logger
from ..objects import MVPObject, OneColorObject, ColoredObject, TexturedObject, Text

ShaderProgram = int

class Shader:
    """Basic Shader class for vertex and fragment shaders. Compiles the raw programs given a tuple of paths."""
    def __init__(self, vertexShader: Union[str, Path], fragmentShader: Union[str, Path]):
        if isinstance(vertexShader, Path):
            vertexShader = open(vertexShader, "r").read()
        if isinstance(fragmentShader, Path):
            fragmentShader = open(fragmentShader, "r").read()
        self.vertexShader = vertexShader
        self.fragmentShader = fragmentShader
        self.shaderProgram = self._compile()

    def _compile(self) -> ShaderProgram:
        vertexShaderCompiled = shaders.compileShader(self.vertexShader, GL_VERTEX_SHADER)
        fragmentShaderCompiled = shaders.compileShader(self.fragmentShader, GL_FRAGMENT_SHADER)
        program = shaders.compileProgram(vertexShaderCompiled, fragmentShaderCompiled)
        return program

    def __repr__(self):
        return self.shaderProgram

def getDefaultShaders() -> Dict[str, Shader]:
    """Instantiates the default shaders, corresponding to the default objects declared in the library."""
    res = {}
    builtIn = [Text, MVPObject, OneColorObject, ColoredObject, TexturedObject]
    for type in builtIn:
        res[type.shaderName] = Shader(type.VS, type.FS)
        logger.debug(f"Initializing built-in shader: '{str(type).split('.')[-1][0:-2]}'.")
    return res
