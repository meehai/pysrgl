from typing import Union, Union
import glfw

_togglers = set()

def _keyMapper(key: str) -> int:
    """Mapper between the string and the glfw integer for said key"""
    Dict = {
        "0": glfw.KEY_0,
        "1": glfw.KEY_1,
        "2": glfw.KEY_2,
        "3": glfw.KEY_3,
        "4": glfw.KEY_4,
        "5": glfw.KEY_5,
        "6": glfw.KEY_6,
        "7": glfw.KEY_7,
        "8": glfw.KEY_8,
        "9": glfw.KEY_9,
        "A": glfw.KEY_A,
        "B": glfw.KEY_B,
        "C": glfw.KEY_C,
        "D": glfw.KEY_D,
        "E": glfw.KEY_E,
        "F": glfw.KEY_F,
        "G": glfw.KEY_G,
        "H": glfw.KEY_H,
        "I": glfw.KEY_I,
        "J": glfw.KEY_J,
        "K": glfw.KEY_K,
        "L": glfw.KEY_L,
        "M": glfw.KEY_M,
        "N": glfw.KEY_N,
        "O": glfw.KEY_O,
        "P": glfw.KEY_P,
        "Q": glfw.KEY_Q,
        "R": glfw.KEY_R,
        "S": glfw.KEY_S,
        "T": glfw.KEY_T,
        "U": glfw.KEY_U,
        "V": glfw.KEY_V,
        "W": glfw.KEY_W,
        "X": glfw.KEY_X,
        "Y": glfw.KEY_Y,
        "Z": glfw.KEY_Z,
        "ESC": glfw.KEY_ESCAPE,
        "UP": glfw.KEY_UP,
        "DOWN": glfw.KEY_DOWN,
        "LEFT": glfw.KEY_LEFT,
        "RIGHT": glfw.KEY_RIGHT
    }
    return Dict[key]

def keyPress(scene, x: Union[int, str]) -> bool:
    """Returns true if the said key was pressed"""
    if isinstance(x, str):
        x = _keyMapper(x)
    return glfw.get_key(scene.window, x) == glfw.PRESS

def keyRelease(scene, x: Union[int, str]) -> bool:
    """Returns true if the said key was released"""
    if isinstance(x, str):
        x = _keyMapper(x)
    return glfw.get_key(scene.window, x) == glfw.RELEASE

def keyPressAndRelease(scene, x: Union[int, str]) -> bool:
    if isinstance(x, str):
        x = _keyMapper(x)

    if keyRelease(scene, x) and x in _togglers:
        _togglers.remove(x)
        return False

    if keyPress(scene, x) and not x in _togglers:
        _togglers.add(x)
        return True

    return False
